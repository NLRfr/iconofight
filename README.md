# IconoFight
> It's [EmojiFight](https://gitlab.com/NLRfr/emojifight) but for the server icon.

## Deploy
- `git clone` the repo
- `composer install`
- import the `schema.sql` in MariaDB
- copy the `.env.example` to `.env` and fill the details
- make sure `.env` is blocked on your webserver
- make sure the webserver can write in `icons/` and `tmp/`
- you're done

## License
MIT
