<?php
class Vote {
  public function beforeRoute($f3) {
    $db = $f3->get('db');
    $f3->set('votes', new DB\SQL\Mapper($db, 'votes'));
  }

  public function submit($f3) {
    $res = 'error';

    try {
      $body = json_decode($f3->get('BODY'));
      $user = $f3->get('SESSION.user')->id;

      $v = $f3->get('votes');
      $v->load(
        array('file = ? AND user = ?', $body->file, $user)
      );

      if ($v->dry()) $v->reset();
      $v->file = $body->file;
      $v->user = $user;
      $v->vote = $body->vote;

      $v->save();
      $res = 'ok';
    } catch (Exception $e) {
      $res = $e->getMessage();
    }

    header('Content-Type: application/json');
    echo json_encode(array(
      'result' => $res
    ));
  }
}
