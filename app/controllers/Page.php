<?php
class Page {
  public function beforeRoute($f3) {
    $db = $f3->get('db');
    $f3->set('icons', new DB\SQL\Mapper($db, 'icons'));
    $f3->set('votes', new DB\SQL\Mapper($db, 'votes'));
  }

  public function home($f3) {
    if ($f3->get('SESSION.user')) {
      $f3->set('default_icon', array(
        'file' => 'icon.png',
        'submitter' => array(
          'id' => '81293337012744192',
          'username' => 'Kody',
          'discriminator' => '1337',
          'flags' => 768
        )
      ));

      $f3->get('icons')->load(
        array('banned != 1')
      );

      $list = [];
      while(!$f3->get('icons')->dry()) {
        $e = $f3->get('icons')->cast();

        $f3->get('votes')->load(
          array('file = ? AND user = ?', $e['file'], $f3->get('SESSION.user')->id)
        );
        if (!$f3->get('votes')->dry())
          $e['vote'] = $f3->get('votes')->vote;
        else
          $e['vote'] = 0;

        $count = 0;
        $f3->get('votes')->load(
          array('file = ?', $e['file'])
        );
        while(!$f3->get('votes')->dry()) {
          $count += $f3->get('votes')->vote;
          $f3->get('votes')->next();
        }
        $e['count'] = $count;

        $list[] = $e;

        $f3->get('icons')->next();
      }
    }

    $f3->set('pageTitle', 'IconoFight');
    $f3->set('list', $list);
    echo \Template::instance()->render('home.htm');

    $f3->clear('SESSION.success');
    $f3->clear('SESSION.error');
  }

  public function error($f3) {
    $f3->set('SESSION.error', '<strong>Erreur.</strong><br>' . $f3->get('ERROR.text'));
    $f3->reroute('/');
  }

  public function login($f3) {
    $red = $f3->get('ENV.MAIN_DOMAIN') . '/login';

    try {
      if ($f3->get('GET.error')) {
        $f3->error(401, 'Discord n\'est pas content.<br>Code : <code>' . $f3->get('GET.error') . '</code>');
      } elseif ($f3->get('GET.code')) {
        $tr = 'https://discordapp.com/api/oauth2/token';

        $t = curl_init();
        curl_setopt_array($t, array(
            CURLOPT_URL => $tr,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array(
                "grant_type" => "authorization_code",
                "client_id" => $f3->get('ENV.DISCORD_ID'),
                "client_secret" => $f3->get('ENV.DISCORD_SECRET'),
                "redirect_uri" => $red,
                "code" => $f3->get('GET.code')
            )
        ));
        curl_setopt($t, CURLOPT_RETURNTRANSFER, true);
        $resp = json_decode(curl_exec($t));
        curl_close($t);

        if (isset($resp->access_token)) {
          $access_token = $resp->access_token;

          $guilds_request = "https://discordapp.com/api/users/@me/guilds";
          $guilds = curl_init();
          curl_setopt_array($guilds, array(
              CURLOPT_URL => $guilds_request,
              CURLOPT_HTTPHEADER => array(
                  "Authorization: Bearer {$access_token}"
              ),
              CURLOPT_RETURNTRANSFER => true
          ));

          $in_server = false;
          foreach (json_decode(curl_exec($guilds)) as $g) {
            if ($g->id == $f3->get('ENV.DISCORD_SERVER'))
              $in_server = true;
          }

          if ($in_server) {
            $info_request = "https://discordapp.com/api/users/@me";
            $info = curl_init();
            curl_setopt_array($info, array(
                CURLOPT_URL => $info_request,
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer {$access_token}"
                ),
                CURLOPT_RETURNTRANSFER => true
            ));

            $f3->set('SESSION.user', json_decode(curl_exec($info)));
            curl_close($info);
            $f3->reroute('/');
          } else {
            $f3->error(403, 'Vous devez être sur le serveur pour participer.');
          }
        } else {
          $f3->error(412, 'L\'authentification semble avoir échoué...');
        }
      } else {
        $a = 'https://discordapp.com/oauth2/authorize';
        $a .= '?client_id=' . $f3->get('ENV.DISCORD_ID');
        $a .= '&redirect_uri=' . urlencode($red);
        $a .= '&response_type=code&scope=identify%20guilds';
        header('Location: ' . $a, true, 302);
      }
    } catch (\Throwable $th) {
      $f3->error(724, 'Quelque chose a planté... Réessayez encore une fois. Si ça persiste, prévenez @Kody#1337.');
    }
  }
}
