<?php
class Submit {
  public function beforeRoute($f3) {
    $db = $f3->get('db');
    $f3->set('icons', new DB\SQL\Mapper($db, 'icons'));
  }

  public function upload($f3) {
    global $source;
    $source = $f3->get('POST.source');
    $e = $f3->get('icons');
    $e->load(
      array('source = ?', $source)
    );

    if (!$f3->get('SESSION.user')) {
      $f3->set('SESSION.error', '<strong>L\'envoi a échoué.</strong><br>Session expirée, veuillez vous reconnecter !');
    } elseif (!$source || empty($source)){
      $f3->set('SESSION.error', '<strong>L\'envoi a échoué.</strong><br>Pas de source ?');
    } elseif (!$e->dry()) {
      $f3->set('SESSION.error', '<strong>L\'envoi a échoué.</strong><br>Cette source existe déjà.');
    } else {
      $e->reset();
      $e->source = $source;
      $e->submitterName = $f3->get('SESSION.user')->username;
      $e->submitterID = $f3->get('SESSION.user')->id;

      $i = 0;
      $web = \Web::instance();
      $files = $web->receive(
        function ($file, $formFieldName) {
          $i++;
          if ($i > 1) return false;

          // TODO: not sure of the correct MIMEs
          if ($formFieldName != 'icon' ||
            $file['type'] != 'image/png' ||
            $file['type'] != 'image/gif' ||
            $file['type'] != 'image/jpeg' ||
            $file['type'] != 'image/webp')
            return false;

          return true;
        },
        false, // no overwrite
        // TODO: this has to be changed
        function ($fileName, $formFieldName) {
          return true;
        }
      );

      foreach ($files as $file) {
        if ($file) {
          // TODO: check if it's the correct $file variable
          $e->file = $file['name'];
          $e->save();
          $f3->set('SESSION.success', '<strong>Voilà !</strong><br>L\'icône est envoyée !');
        } else {
          $f3->set('SESSION.error', '<strong>L\'envoi a échoué.</strong><br>Est-ce bien une image valide ?');
        }
      }
    }

    $f3->reroute('/');
  }
}
