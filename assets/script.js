/* global FileReader */
/* A toi qui lis ce fichier, je suis désolé du spaghetti. */

function voteSubmit () {
  var body = {
    icon: this.dataset.icon,
    vote: parseInt(this.dataset.vote)
  }

  var v = document.querySelector('article[data-icon="' + this.dataset.icon + '"] .details button' + (this.dataset.vote > 0 ? '.is-success' : '.is-danger'))
  var o = document.querySelector('article[data-icon="' + this.dataset.icon + '"] .details button' + (this.dataset.vote < 0 ? '.is-success' : '.is-danger'))

  v.disabled = true
  o.disabled = true
  v.classList.add('is-loading')

  window.fetch('/vote', {
    method: 'POST',
    body: JSON.stringify(body)
  }).then(res => {
    return res.json()
  }).then(json => {
    setTimeout(_ => {
      if (json.result === 'ok') {
        v.classList.remove('is-outlined')
        o.classList.add('is-outlined')
      }
      v.disabled = false
      o.disabled = false
      v.classList.remove('is-loading')
    }, 1000)
  })
}

document.querySelectorAll('article .details button').forEach(el => {
  el.addEventListener('click', voteSubmit)
})

if (typeof FileReader === 'function') {
  document.querySelector('.form-preview').style.display = 'block'
  var fr = new FileReader()
  fr.onload = function () {
    var res = this.result
    document.querySelectorAll('.form-preview img').forEach(el => {
      el.src = res
    })
  }
}

document.querySelector('.modal .file-input').onchange = function () {
  if (this.files[0]['type'] === 'image/png') {
    document.querySelector('.modal .file-name').innerHTML = this.files[0]['name']
    document.querySelector('.modal button[type=submit]').disabled = false
    var t = document.querySelector('.modal #name')
    if (t.value === '') t.value = this.files[0]['name'].replace(/\.[^/.]+$/, '').substring(0, 32)
    if (typeof fr === 'object') fr.readAsDataURL(this.files[0])
  } else {
    document.querySelector('.modal button[type=submit]').disabled = true
  }
}

document.querySelectorAll('.modal .close-modal').forEach(el => {
  el.addEventListener('click', _ => {
    document.querySelector('.modal').classList.remove('is-active')
  })
})

document.querySelectorAll('.open-modal').forEach(el => {
  el.addEventListener('click', _ => {
    document.querySelector('.modal').classList.add('is-active')
  })
})
